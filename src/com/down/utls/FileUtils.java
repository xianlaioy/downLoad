package com.down.utls;

import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;

public class FileUtils {
	public static String getfilename(String urlt) {
		String fileName = urlt.split("//")[1].split("/")[urlt.split("//")[1].split("/").length - 1];
		System.out.println(fileName);

		return fileName;
	}

	public static String getfilesize(String urlt) {
		// System.out.println(fileName);
		URL url;
		String m = null;
		try {
			url = new URL(urlt);
			HttpURLConnection http = (HttpURLConnection) url.openConnection();
			double k = (http.getContentLengthLong() / 1024.0) / 1024;

			DecimalFormat df = new DecimalFormat("######0.00");
			m = df.format(k);
			System.out.println("file size:" + m + "MB");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/**
		 * 此处设定5个线程下载一个文件tn = 5; 判断平均每个线程需下载文件长度：
		 */

		return m + "MB";
	}

	public static void main(String[] args) {
		getfilename("http://wap.apk.anzhi.com/data2/apk/201605/24/8a4181ec3ab56cb6084fbe71a39501bb_74636000.apk");

		getfilesize("http://wap.apk.anzhi.com/data2/apk/201604/29/3886095eadc44f40a8a2ed9c1cd235f9_24923300.apk");
	}
}
